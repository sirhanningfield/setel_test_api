'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('settings', [{
        update_frequency: 5,
        createdAt: Date.now(),
        updatedAt: Date.now()
      }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('settings', null, {});
  }
};