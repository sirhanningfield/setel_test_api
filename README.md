## Setup

1. run npm install
2. change mysql config in config/config.json
3. run sequelize db:migrate (make sure mysql server is running)
4. run sequelize db:seed:undo:all (this step inserts a frequency in the settings table)
5. run nodemon start

## Assumptions
1. App considers only 1 user.
2. user Authentication is mocked in payment call
3. payment process is mocked and status "confirmed" or "declined" is randomly returned
