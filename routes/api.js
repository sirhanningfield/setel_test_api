var express = require('express');
var router = express.Router();
var ordersController = require("../controllers/ordersController");
var paymentsController = require("../controllers/paymentsController");
const auth = require('../middleware/auth');

/* ORDERS*/
router.get('/orders/health', ordersController.healthCheck);

/* ORDERS*/
router.put('/orders/frequency', ordersController.updateFrequency);
router.post('/orders', ordersController.createOrder);
router.get('/orders', ordersController.getOrders);
router.get('/orders/frequency', ordersController.getFrequency);
router.post('/orders/status', ordersController.updateOrderStatus);
router.get('/orders/:order_id', ordersController.getOrder);


/* PAYMENTS */
router.get('/payments/health', paymentsController.healthCheck);
router.post('/payments', auth, paymentsController.createPayment);




module.exports = router;