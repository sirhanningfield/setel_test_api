'use strict';
module.exports = (sequelize, DataTypes) => {
  const orders = sequelize.define('orders', {
    order_no: DataTypes.STRING,
    item_name : DataTypes.STRING,
    amount : DataTypes.FLOAT,
    status : DataTypes.STRING
  }, {});
  orders.associate = function(models) {
    // associations can be defined here
  };
  return orders;
};