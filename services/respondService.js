
exports.respond = function(res, payload, code){

    if(!code) code = 200;
    let params = {
        status : 1,
        message : "success",
        data : payload
    }

    res.json(params,code);
}

exports.respondError = function (res, payload, code, message){
    if(!code) code = 500;
    if(!message) message = "Internal Error";
    if(!payload) payload = {};

    let params = {
        status : 0,
        error_message : message,
        error_code : code,
        error_data : payload
    }

    res.json(params,code);
}