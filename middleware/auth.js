const responseService = require('../services/respondService');
module.exports = function(req,res,next){
    
    if(req.headers.authorization){
        var token = req.headers.authorization.split(" ")[1]
        if (token != "mockToken") return responseService.respondError(res,null,null,"invalid token")
        next();
    }else{
        return responseService.respondError(res,null,null,"missing token")
    }
    
}