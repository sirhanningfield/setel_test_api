const db = require('../models');
const responseService = require('../services/respondService');

exports.createPayment = (req, res) => {
    req.check('order_id', 'order id is required').notEmpty();
    var errors = req.validationErrors();
    if(errors) responseService.respondError(res, null, 400, errors[0].msg);

    var order_id = req.body.order_id;

    // Mock the response of the payment using a random number and some criteria
    var random = Math.random() * (5 - 1) + 1;

    if (random < 2) {
        status = "declined";
    }else{
        status = "confirmed";
    }

    params = {
        payment : {
            order_id : order_id,
            status : status
        }
    }
    
    return responseService.respond(res, params);
}

exports.healthCheck = (req, res) => {
    params = {
        status : "LIVE"
    }
    return responseService.respond(res, params);
}