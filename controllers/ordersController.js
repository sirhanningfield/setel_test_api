const db = require('../models');
const responseService = require('../services/respondService');



// PUT Update frequency of order (in sec)
exports.updateFrequency = function(req, res){
    req.check('frequency', 'frequency is required').notEmpty();
    var errors = req.validationErrors();
    if(errors) responseService.respondError(res, null, 400, errors[0].msg);

    var frequency = req.body.frequency;

    // update the frequency based on the input
    db.settings.findOne({
        where : {
            id : 1
        }
    }).then((setting)=>{
        if (!setting) {
            return responseService.respondError(res, null, 404, "frequency setting not found.");
        }
        
        setting.update({
            update_frequency : frequency
        }).then((updatedFrequency) => {
            var params = {
                frequency : updatedFrequency.update_frequency
            }
            return responseService.respond(res,params);
        }).catch((err) => {
            return responseService.respondError(res, err);
        })
    }).catch((err) => {
        return responseService.respondError(res, err);
    })
    
}


// POST create a new order
exports.createOrder = (req, res) => {

    // valdiate body inputs
    req.check('order_no', 'order_no is required').notEmpty();
    req.check('item_name', 'item_name is required').notEmpty();
    req.check('amount', 'amount is required').notEmpty().isNumeric();
    var errors = req.validationErrors();
    if(errors) responseService.respondError(res, null, 400, errors[0].msg);

    var order_no = req.body.order_no;
    var item_name = req.body.item_name;
    var amount = req.body.amount;
    

    // IF an order_no already exists
    db.orders.findOne({
        where : {
            order_no : order_no
        }
    }).then((order)=>{
        if(order) return responseService.respondError(res, null, 403, "Order no already used.");
        db.orders.create({
            order_no : order_no,
            item_name : item_name,
            amount : amount,
            status : "created"
        }).then((createdOrder)=>{
            params = {
                order : createdOrder
            }
            return responseService.respond(res, params, 201, "Order created successfully.");
        }).catch((err) => {
            return responseService.respondError(res, err);
        })
    }).catch((err) => {
        return responseService.respondError(res, err);
    })

}


// GET all orders
exports.getOrders = (req, res) => {
    db.orders.findAll()
    .then((orders) => {
        var orders = {
            orders : orders
        }
        return responseService.respond(res, orders);
    }).catch((err) => {
        return responseService.respondError(res, err);
    })
} 


// GET Order details
exports.getOrder = (req, res) => {

    // Validate query input
    order_id = req.params.order_id;
    if(!order_id) return responseService.respondError(res, null, 400, "Order Id not found.");

    db.orders.findOne({
        where : {
            id : order_id
        }
    }).then((order) => {
        if(!order) return responseService.respondError(res, null, 400, "Invalid Order Id.");
        var params = {
            order : order
        }
        return responseService.respond(res, params);
    }).catch((err) => {
        return responseService.respondError(res, err);
    })

}

exports.updateOrderStatus = (req, res) => {

    // Validate inputs
    req.check('order_id', 'order_no is required').notEmpty();
    req.check('status', 'status is required').notEmpty();
    var errors = req.validationErrors();
    if(errors) responseService.respondError(res, null, 400, errors[0].msg);

    var order_id = req.body.order_id;
    var status = req.body.status;

    // Get the frequency
    var frequency ;
    db.settings.findOne({
        where : {
            id : 1
        }
    }).then((setting) => {
        if(!setting) responseService.respondError(res);
        frequency = setting.update_frequency * 1000;
    }).then(()=>{
        db.orders.findOne({
            where : {
                id : order_id
            }
        }).then((order)=> {
            if(!order) responseService.respondError(res, null, 404, "Invalid order id.");
            switch (status) {
                case "cancelled":
                    order.update({
                        status : status
                    }).then((updatedOrder) => {
                        var params = {
                            order : updatedOrder
                        }
                        return responseService.respond(res, params);
                    }).catch((err) => {
                        return responseService.respondError(res, err.msg);
                    })
                    break;
                case "confirmed":
                    // Changed order status to confirmed
                    order.update({
                        status : status
                    }).then((updatedOrder) => {
                        var params = {
                            order : updatedOrder
                        }
                        // Then after a set amount of seconds change status to delivered
                        setTimeout(function () {
                            console.log("updating to delivered...");
                            updatedOrder.update({
                                status : "delivered"
                            })

                        }, frequency); 

                        return responseService.respond(res, params);
                    }).catch((err) => {
                        responseService.respondError(res, err.msg);
                    })
                    // Then after a set amount of seconds change status to delivered
    
                    break;
                case "declined":
                    // Change status to cancelled
                    order.update({
                        status : "cancelled"
                    }).then((updatedOrder)=>{
                        var params = {
                            order : updatedOrder
                        }
                        return responseService.respond(res, params);
                    }).catch(()=>{
                        return responseService.respondError(res, null,null,"cannot update status.");
                    })
                    break;
                default:
                    responseService.respondError(res, null,null,"Invalid status provided.");
                    break;
            }
        })
    })
}

exports.getFrequency = (req, res) => {
    console.log("here");
    var frequency;
    db.settings.findOne({
        where : {
            id : 1
        }
    }).then((setting) => {
        frequency = setting.update_frequency;
        var params = {
            frequency : frequency
        }
        console.log(frequency);
        return responseService.respond(res, params);
    })
}

exports.healthCheck = (req, res) => {
    params = {
        status : "LIVE"
    }
    return responseService.respond(res, params);
}